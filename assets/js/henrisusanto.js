jQuery(document).ready(function(){
    var source = new EventSource(base_url+'index.php/phone/popup_drivers_sms');
    source.onmessage = function(event) {
        document.getElementById("sms_popup").innerHTML = event.data + "<br>";
    };
   
   // SSE on line-box
    var source = new EventSource(base_url+'index.php/main/sse');
    source.onmessage = function(event) {
        data = JSON.parse(event.data);
        document.getElementById("line-box").innerHTML = data.line;
        document.getElementById("road-box").innerHTML = data.road;
        setTimer('road-box');
        document.getElementById("hill-box").innerHTML = data.hill;
        document.getElementById("air-box").innerHTML = data.airport;
        document.getElementById("trans-box").innerHTML = data.trans;
        setTimer('trans-box');
        document.getElementById("unoff-box").innerHTML = data.unoffice;

        jQuery('fieldset legend :checkbox:checked').each(function(){
          jQuery(this).parent().parent().find('li :checkbox').click();
        });

        var getIncludeme = localStorage.getItem('includeme');
        var includeme = getIncludeme===null?new Array():JSON.parse(getIncludeme);
        // document.getElementById("localStorage").innerHTML = getIncludeme;

        if(includeme.length>0)
          for(check=0; check<includeme.length; check++)
            jQuery(':checkbox[value="'+includeme[check]+'"]').prop('checked', true);

        jQuery(':checkbox').click(function(){
          var val = jQuery(this).val();
          if(this.checked){
            includeme[includeme.length] = val;
          }else{
            var index = includeme.indexOf(val);
            if(index!==-1) includeme.splice(index, 1);
          }
          localStorage.setItem('includeme',JSON.stringify(includeme));
        });
        
       jQuery('[name="out of service"]').click(function(){
           if(jQuery('#insert_cab_name').val()!=='') return true;
           else{
               massive_out_of_service();
               return true;
           }
           return false;
       });
};
    
   jQuery('#button_rotate').click(function(){
       var popup = jQuery('#choose_position');
       popup.dialog();
       popup.find(':radio').click(function(){
           var value = jQuery(this).val();
           jQuery('#form_cab').append('<input type="hidden" name="position" value="'+value+'">');
           jQuery('#form_cab').submit();
       });
       return false;
   });
   
   jQuery('#btn_phone_number').click(function(){
        var target = jQuery('#phone_number');
        var cname = jQuery('#insert_cab_name').val();
        if (cname == '') return false;
        target.find('input[name="name"]').val(cname);
        jQuery.ajax({
            url: base_url + '/index.php/phone/getPhoneNumber/' + cname,
            type: 'GET',
            dataType: 'text',
            success: function(number) {
                target.find('input[name="phone_number"]').val(number);
                target.dialog({title:'Cab Number '+cname});
                target.find('[type="submit"]').click(function(){
                    target.find('form').submit();
                });
            }
        });
        return false;
    });
});

function setTimer(location/* road or trans */){
   var container = jQuery('#'+location);
   container.find('li').each(function() {
       var t = new Date();
       var link = jQuery(this).find('a');
        t.setMinutes(t.getMinutes() + parseInt(link.attr('data-minute')));
        t.setSeconds(t.getSeconds() + parseInt(link.attr('data-second')));
        jQuery(this).find('div.countdowntimer').countdown({until: t,
            layout: '<div><b>{hn}{sep}{mnn}{sep}{snn}</b></div>',
            expiryUrl:base_url
        });
   });
}

function unofficial(cname)
{
    var unofficial = jQuery('div#unofficial');
    unofficial.dialog();
    unofficial.find(':radio').click(function(){
        var destin = jQuery(this).val();
        window.location = base_url+'index.php/main/cabgoto/'+cname+'/'+destin;
    });
}

function massive_out_of_service(){
    var recs = 0;
    var formcab = jQuery('#form_cab');
    jQuery('[name="includeme"]:checked').each(function(){
        var cname = jQuery(this).val();
        formcab.append('<input type="hidden" name="name[]" value="'+cname+'">');
        recs++;
    });
    if(recs<1) return;
    else formcab.find('[name="name"]').attr('disabled','disabled');
    formcab.submit();
}

function send_sms(){
    var dialog = jQuery('#send_sms');
    var recs = 0;
    jQuery('[name="includeme"]:checked').each(function(){
        var cname = jQuery(this).val();
        dialog.find('form').append('<input type="hidden" name="receiver[]" value="'+cname+'">');
        dialog.find('span').append(cname+', ');
        recs++;
    });
    if(recs<1) return;
    dialog.dialog();
}