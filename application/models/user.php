<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Model {
    
    protected $table = 'user';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function select($item = null, $list = false)
    {
        if(isset($item['password'])) $item['password'] = md5 ($item['password']);
        $query = ($item == null) ? 
                $this->db->order_by('id','desc')->get($this->table) : 
                $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
}
