<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class variable extends CI_Model {
    
    protected $table = 'variable';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function select($item = null, $list = false)
    {
        $query = ($item == null) ? 
                $this->db->order_by('id','desc')->get($this->table) : 
                $this->db->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
    public function update($var){
//        die(var_dump($var));
        $this->db->set('value',$var['value'])->where('variable',$var['variable'])->update($this->table);
    }
    
    public function getSmsTemplate(){
        return $this->db->like('variable','sms')->get($this->table)->result();
    }
    
}
