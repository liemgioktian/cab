<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cab extends CI_Model {
    
    protected $table = 'cab';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function select($item = null, $list = false)
    {
        $query = ($item == null) ? 
                $this->db->order_by('cid','desc')->get($this->table) : 
                $this->db->order_by('cid','desc')->get_where($this->table, $item);
        return ($list) ? $query->result() : $query->row_array();
    }
    
    public function getList($location){
        return $this->db
                ->order_by('queue','desc')
                ->where('position',$location)
                ->get($this->table)
                ->result();
    }
    
    public function update($cab){
//        if(isset($cab['position'])){
//            $exist = $this->select(array('name'=>$cab['name']),false);
//            if($exist['position']!=$cab['position'])
//                $cab['moved'] = (int)$exist['moved']+1;
//        }
        if($cab['position']==2||$cab['position']==6){
            $start = new DateTime();
            $cab['start'] = $start->format('Y-m-d H:i:s');
        }
        $this->db->where('name',$cab['name'])->update($this->table,$cab);
        return true;
    }
    
    public function add($cab)
    {
        $exist = $this->select(array('name'=>$cab['name']),false);
        if(empty($exist) && $cab['name']!=''){
            $cab['queue'] = (int)$this->getMaxQueue(1) +1;
            $this->db->insert($this->table,$cab);
        }
    }
    
    public function rotate($cab, $position)
    {
        $cab['position'] = $position; /*Set the Position in variable which requested */
        $exist = $this->select(array('name' => $cab['name']), false); /*checking curent exitance in DB */
        if (empty($exist)) return false; /* If not exists return false */
        if ($exist['position'] != $cab['position']){ /* if the position of exists is not same as proposed */
            $cab['queue'] = (int) $this->getMaxQueue($cab['position']) + 1;/* GET THE NEXT POSITON MAX QUE */
            // prevent leaved place to be empty
            $old_queue_in_old_location = $exist['queue']; /* OLD POSITION QUE PICKUP */
            $max_queue_in_old_location = (int) $this->getMaxQueue($exist['position']);  /* OLD POSITION MAX QUE PICKUP */
            if($old_queue_in_old_location!=$max_queue_in_old_location){    /* IF OLD POSITION MAX QUE PICKUP  IS NOT SAME OLD POSITIN MY QUE*/
                $this->db
                        ->set('queue', '`queue`-1', FALSE)    /* UPDATE QUE TO MIN 1 TO ALL OF MY OLD POSITION WHERE  queue > old_queue_in_old_location */
                        ->where('position', $exist['position'])
                        ->where('queue >', $old_queue_in_old_location)
                        ->update($this->table);
            }
        }else {
            if ($exist['queue'] != 1) return false;// allow rotation in single location only for lowest cab
            $cab['queue'] = (int) $this->getMaxQueue($cab['position']);
            $this->db
                    ->set('queue', '`queue`-1', FALSE)
                    ->where('position', $cab['position'])
                    ->update($this->table);
        }
        
        $this->update($cab);
        if ($exist['position'] != $cab['position']) $this->autosend_triggered_sms($cab, $exist['position']);
    }
    
    // METHOD THAT BREAKING THE RULES, USING CODE FROM CONTROLLER phone AND MODEL messaging
    function autosend_triggered_sms($cab, $old_position/*, $trigger contains variable name in table variable */){
        $this->load->model('variable');
        $sending_sms = $this->variable->select(array('variable'=>'sending_sms'),false);
        if($sending_sms['value']=='off') return;
        
        $this->load->library('twilio');
        $this->config->load('twilio');
        
        if(!isset($cab['phone']))
            $cab = $this->select (array('name'=>$cab['name']),false);
        
        $sms['receiver'] = $cab['phone'];
        if(strlen($sms['receiver'])<1) return false;

        $name = $cab['name'];
        $source = $old_position;
        $destination = $cab['position'];
        $sms['body']="";//"$source, $destination";
        if($source==1 && $destination==2)
            $sms['body'] = "Car number $name, You have 25 minute to get to the Hill";
        else if($source==2 && $destination==3){
            $status = (int)$this->getMaxQueue(3);
            $sms['body'] = "Car number $name, you are $status on the hill";
        }else if($source==3 && $destination==6){
            $sms['body'] = "Car number $name, You have 7 minute to get to the Airport";
        }else if($source==4 && $destination==1){// OUTAIRPORT
            $status = (int)$this->getMaxQueue(1);
            $sms['body'] = "Car Number $name out of the airport and back $status in line up !";
        }else if($source==0 && $destination==1){// LINE
            $status = (int)$this->getMaxQueue(1);
            $sms['body'] = "Car Number $name you are $status in the line up. Thank you !";
        }else if($destination==0){
        	$sms['body'] = "Car number $name you have been put out of service";
        }else if($destination==5){
        	$status = (int)$this->getMaxQueue(5);
        	$sms['body'] = "Car number $name you have been move $status unofficial";
        }
        
        if(strlen($sms['body'])=='') return false;
        $response = $this->twilio->sms($this->config->item('number'), $sms['receiver'], $sms['body']);
        if ($response->IsError){
            $sms['body'] = json_encode($response);
            $this->session->set_flashdata('notif','Failed send message to '.$sms['receiver']);
        }
        $sent = new DateTime();
        $sms['sent'] = $sent->format('Y-m-d H:i:s');
        $this->db->insert('outgoing_sms', $sms);
        return true;
    }
    
    public function check_status($cab)
    {
        $cab_name = $cab['name'];
        $exist = $this->select(array('name'=>$cab_name),false);
        if(empty($exist)) return 'Cab not found';
        $reply = "Cab number $cab_name status is ";
        $reply.= $this->translatePosition($exist['position']);
        $in_location = $this->select(array('position'=>$exist['position']), true);
        $position = $exist['queue'];
        $reply .= ", position $position";
        return $reply;
    }
    
    public function unofficial_hill ($cab)
    {
        $this->rotate($cab,5);
    }
    
    public function out_of_service($cab)
    {
//        $this->db->where('name',$cab['name'])->delete($this->table);
        $this->rotate($cab, 0);
    }
    
    public function getMaxQueue($location){
        return count($this->getList($location));
    }
    
    public function getFirstQueue($position){
        return $this->db->get_where($this->table, array('queue'=>1,'position'=>$position))->row_array();
    }

    public function translatePosition($int){
        switch ($int) {
            case 0: return 'OUT OF SERVICE'; break;            
            case 1: return 'LINE'; break;            
            case 2: return 'ROAD'; break;            
            case 3: return 'HILL'; break;            
            case 4: return 'AIRPORT'; break;            
            case 5: return 'UNOFFICIAL'; break;            
            case 6: return 'TRANSITION'; break;            
            default: return 'UNDEFINED'; break;
        }
    }
		
		public function validateQueue () {
			$conflictedGroups = $this->db->query(
				"SELECT DISTINCT a.position FROM cab a
				INNER JOIN cab b ON a.position = b.position AND a.queue = b.queue
				WHERE a.cid <> b.cid OR a.queue = 0")->result();
			foreach ($conflictedGroups as $position) {
				$cabs = $this->getList($position->position);
				$cabsCount = count($cabs); 
				foreach ($cabs as $cab) {
					$cab->queue = $cabsCount;
					$this->update((array)$cab);
					$cabsCount -- ;
				}
			}
		}
}
