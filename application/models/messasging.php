<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class messasging extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function getList($table, $filter=null, $limit=null, $offset=0){
        if(!is_null($limit))$this->db->limit($limit, $offset);
        if(!is_null($filter))$this->db->where($filter);
        return $this->db->get($table)->result();
    }
    
    function getSms($table,$sms){
        return $this->db->get_where($table, $sms)->row_array();
    }
    
    function deleteSms($table,$sms){
        $this->db->where($sms)->delete($table);
    }
    
    function storeSms($table,$sms){
        $stored = new DateTime();
        if($table=='incoming_sms'){
            $parsed['sender'] = $sms['From'];
            $parsed['body'] = $sms['Body'];
            $parsed['received'] = $stored->format('Y-m-d H:i:s');
        }else{
            $parsed['receiver'] = $sms['receiver'];
            $parsed['body'] = $sms['body'];
            $parsed['sent'] = $stored->format('Y-m-d H:i:s');
        }
        $this->db->insert($table,$parsed);
    }

    function getLatestIncoming(){
        return $this->db->get('incoming_sms',1,0)->row_array();
    }
}
