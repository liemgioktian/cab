<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    public function index() {
        $this->load->model('variable');
        $notice = $this->variable->select(array('variable'=>'notice'),false);
        $data['notice'] = !empty($notice)?$notice['value']:'';
        $sending_sms = $this->variable->select(array('variable'=>'sending_sms'),false);
        $data['sending_sms'] = $sending_sms['value'];


        $forwarding_sms = $this->variable->select(array('variable'=>'forwarding_sms'),false);
        $data['forwarding_sms'] = $forwarding_sms['value'];

        $forwarding_number = $this->variable->select(array('variable'=>'forwarding_number'),false);
        $data['forwarding_number'] = $forwarding_number['value'];
        
        
        $data['notif'] = (!$this->session->flashdata('notif'))?
                '':$this->session->flashdata('notif');
        
        $data['sms_templates_button'] = $this->variable->getSmsTemplate();
        $this->load->view('main',$data);
    }

    public function setvar()
    {
        $post = $this->input->post();
        $this->load->model('variable');
        $this->variable->update($post);
        redirect(base_url());
    }
    
    public function getvar($variable){// ajax
        $this->load->model('variable');
        $record = $this->variable->select(array('variable'=>$variable),false);
        echo empty($record)?'':$record['value'];
    }
    
    public function cabs()
    {
        $post = $this->input->post();
        if($this->session->userdata('uid')){
            $this->load->model('cab');
            $cab['name'] = $post['name'];
            if(isset($post['position'])) $this->cab->rotate($cab, $post['position']);
            else
                foreach(array('add',/*'rotate',*/'check_status','unofficial_hill','out_of_service') as $submit){
                    if(isset($post[$submit])) {
                        if(is_array($post['name'])){
                            foreach ($post['name'] as $name){
                                $cab['name'] = $name;
                                $result = $this->cab->$submit($cab);
                            }
                        }
                        else $result = $this->cab->$submit($cab);
                        if($submit=='check_status') $this->session->set_flashdata('notif',$result);
                    }
                }
        }
        redirect(base_url());
    }
    
    public function dispatch(){
        $post = $this->input->post();
        $this->load->model('cab');
        if($this->session->userdata('uid')){
            if(isset($post['delete'])){
                $this->cab->out_of_service(array('name'=>$post['name']));
            }else if(isset ($post['dispatch'])){
                $this->cab->rotate(array('name'=>$post['name']), 2);
            }
        }
        redirect(base_url());
    }
    
    public function cabgoto($cname, $destination){
        if($this->session->userdata('uid')){
            $this->load->model('cab');
            $this->cab->rotate(array('name'=>$cname),$destination);
        }
        redirect(base_url());
    }
    
    public function sse(){
    	$this->load->model('cab');
        while (true) {
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');
            
            $cabs = array(
                'line'=>'',
                'road'=>'',
                'hill'=>'',
                'trans'=>'',
                'airport'=>'',
                'unoffice'=>''
            );
            for($position=1; $position<=6; $position++){
                foreach($this->cab->getList($position) as $cab){
                    switch ($position) {
                        case 1: 
                            $pos = 'line'; 
                            $href = site_url("main/cabgoto/$cab->name/2");
                            break;
                        case 2: 
                            $pos = 'road'; 
                            $href = site_url("main/cabgoto/$cab->name/3");
                            break;
                        case 3: 
                            $pos = 'hill'; 
                            $href = site_url("main/cabgoto/$cab->name/6");
                            break;
                        case 6: 
                            $pos = 'trans'; 
                            $href = site_url("main/cabgoto/$cab->name/4");
                            break;
                        case 4: 
                            $pos = 'airport'; 
                            $href = site_url("main/cabgoto/$cab->name/1");
                            break;
                        case 5: 
                            $pos = 'unoffice'; 
                            break;
                    }
                    $href = $this->session->userdata('uid')?$href:'javascript:;';
                    switch ($position) {
                        case 1:
                        case 3:
                        case 4:
                           $cabs[$pos].= '<li>';
                           $cabs[$pos].= "<a title='$cab->phone' href='$href'>";
                           $cabs[$pos].= $cab->name;
                           $cabs[$pos].= '</a>';
                           $cabs[$pos].= "<span>Status:$cab->queue</span>";
                           $cabs[$pos].= "<input type='checkbox' name='includeme' value='$cab->name'>";
                           $cabs[$pos].= '</li>';
                            break;
                        case 5:
                           $cabs[$pos].= '<li>';
                           $cabs[$pos].= "<a title='$cab->phone' href=\"javascript:unofficial('$cab->name');\">";
                           $cabs[$pos].= $cab->name;
                           $cabs[$pos].= '</a>';
                           $cabs[$pos].= "<span>Status:$cab->queue</span>";
                           $cabs[$pos].= "<input type='checkbox' name='includeme' value='$cab->name'>";
                           $cabs[$pos].= '</li>';
                            break;
                        case 2:
                            $now = new DateTime();
                            $until = new DateTime($cab->start);
                            $start = clone($until);
                            $until->add(new DateInterval("PT18M"));
                            $diff = $now->diff($until, true);
                            if ($until <= $now) {
                                $this->cab->rotate(array('name' => $cab->name), 1);
                            } else {
                               $cabs[$pos].= '<li>';
                               $cabs[$pos].= "<a title='$cab->phone' href='$href' data-minute='$diff->i' data-second='$diff->s'>";
                               $cabs[$pos].= $cab->name;
                               $cabs[$pos].= '</a>';
                               $cabs[$pos].= "<span>Status:$cab->queue</span>";
                               $cabs[$pos].= "<input type='checkbox' name='includeme' value='$cab->name'>";
                               $cabs[$pos].= '<br/><div class="countdowntimer"></div>';
                               $cabs[$pos].= '</li>';
                            }
                            break;
                        case 6:
                            $now = new DateTime();
                            $until = new DateTime($cab->start);
                            $start = clone($until);
                            $until->add(new DateInterval("PT8M"));
                            $diff = $now->diff($until, true);
                            if ($until <= $now) {
                                $this->cab->rotate(array('name' => $cab->name), 1);
                            } else {
                               $cabs[$pos].= '<li>';
                               $cabs[$pos].= "<a title='$cab->phone' href='$href' data-minute='$diff->i' data-second='$diff->s'>";
                               $cabs[$pos].= $cab->name;
                               $cabs[$pos].= '</a>';
                               $cabs[$pos].= "<input type='checkbox' name='includeme' value='$cab->name'>";
                               $cabs[$pos].= "<span>Status:$cab->queue</span>";
                               $cabs[$pos].= '<br/><div class="countdowntimer"></div>';
                               $cabs[$pos].= '</li>';
                            }
                            break;
                    }
                }
                
            }
            print "Event: server-time" . PHP_EOL;
            print "data: ". json_encode($cabs) . PHP_EOL;
            print PHP_EOL;

            ob_end_flush();
            flush();
            sleep(1);
        }
    }

		public function reform () {
			$this->load->model('cab');
			$this->cab->validateQueue();
			return true;
		}
}