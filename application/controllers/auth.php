<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {
        if(!$this->session->userdata('uid')){// login
            $post = $this->input->post();
            $this->load->model('user');
            $user = $this->user->select($post,false);
            if(!empty($user)) $this->session->set_userdata($user);
        }else{// logout
            $this->session->sess_destroy();
        }
        redirect(base_url());
    }

}