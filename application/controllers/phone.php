<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class phone extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function send_sms() {
        $post = $this->input->post();
        if(is_array($post['receiver'])){
            foreach($post['receiver'] as $receiver){
                $sms['receiver'] = $receiver;
                $sms['body'] = $post['body'];
                $this->send_single_sms($sms);
            }
        }else $this->send_single_sms ($post);
        redirect(base_url());
    }
    
    function send_single_sms($sms){
        $this->load->model('variable');
        $sending_sms = $this->variable->select(array('variable'=>'sending_sms'),false);
        if($sending_sms['value']=='off') return;       

        $this->load->library('twilio');
        $this->config->load('twilio');
        $this->load->model('cab');
        
        $cab = $this->cab->select(array('name'=>$sms['receiver']),false);
        $sms['receiver'] = $cab['phone'];
        if(strlen($sms['receiver'])<1 || strlen($sms['body'])<1) return false;
        
        $response = $this->twilio->sms($this->config->item('number'), $sms['receiver'], $sms['body']);
        $this->load->model('messasging');
        if (!$response->IsError){
            $this->messasging->storeSms('outgoing_sms',$sms);
        }else{
            $sms['body'] = json_encode($response);
            $this->messasging->storeSms('outgoing_sms',$sms);
            $this->session->set_flashdata('notif','Failed send a message to '.$sms['receiver']);
        }
    }
    
    function store_incoming_sms() {
        $post = $this->input->post();
        $this->load->model('messasging');
        $this->messasging->storeSms('incoming_sms',$post);
        $this->load->model('cab');
        $cab = $this->cab->select(array('phone'=>$post['From']),false);
		$this->load->model('variable');      
		$forwarding_sms = $this->variable->select(array('variable'=>'forwarding_sms'),false);
        $forwarding_number = $this->variable->select(array('variable'=>'forwarding_number'),false);
        if(!empty($cab)){
            $this->sms_command($post,$cab);
        }else{
        	header("content-type: text/xml");
	        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	        ?>
		        <Response>
		            <Message>Sorry, our system can not identify your phone number</Message>
		        </Response>
	        <?php
        }
		if(!empty($cab) && $forwarding_sms['value']=='on' ){
            header('Content-Type: text/html');
	        ?>
Message Sent!
	        <?php
        }
    }
    
    function popup_drivers_sms(){
    	$this->load->database();
        while (true) {
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');

            $sms = $this->db
                    ->select('incoming_sms.*')
                    ->select('cab.name')
                    ->from('incoming_sms')
                    ->join('cab','incoming_sms.sender=cab.phone','left')
                    ->limit(9)
                    ->offset(0)
                    ->order_by('received','DESC')
                    ->get()
                    ->result();$popup = '<div class="col-xs-12 col-md-4">';
			$i = 0;
            foreach($sms as $msg){
                $popup .= '<b><span class="red">'.$msg->name.' sent : </span>'.$msg->body.' on '.$msg->received.'</b><br/>';

				if ($i == 2 ) { 
			       $popup .= '</div><div class="col-xs-12 col-md-4">';
			        $i = 0;
			    } else {
			        $i++;
			    }
			}
			$popup .= '</div>';
            print "Event: server-time" . PHP_EOL;
            print "data: " .$popup. PHP_EOL;
            print PHP_EOL;

            ob_end_flush();     // Strange behaviour, will not work
            flush();            // Unless both are called !
            sleep(1);
        }
    }

    function sms_command($sms,$cab){
        switch (strtoupper($sms['Body'])) {
            case 'STATUS':
                $send['receiver'] = $cab['name'];
                $send['body'] = $this->cab->check_status($cab);
                $this->send_single_sms($send);
                break;
            case 'LINE': 
                if($cab['position']!=1)
                $this->cab->rotate(array('name'=>$cab['name']),1); 
                break;
            case 'HILL': 
                if($cab['position']==2)
                $this->cab->rotate(array('name'=>$cab['name']),3); 
                break;
			 case 'IN': 
                if($cab['position']==6)
                $this->cab->rotate(array('name'=>$cab['name']),4); 
                break;
			case 'ROTATE':
				 $this->cab->rotate(array('name'=>$cab['name']),1);
			break;

			case 'OFF':
				$this->cab->out_of_service(array('name'=>$cab['name']));
				break;

            case 'OUT':
                if($cab['position']!=4) return false;
                $this->cab->rotate(array('name'=>$cab['name']),1);// send him to line
                
                $first_inline = $this->cab->getFirstQueue(1);
                if($this->cab->getMaxQueue(1)>1)
                	$this->cab->rotate(array('name'=>$first_inline['name']),2); // send first queue to road
                
                $first_inroad = $this->cab->getFirstQueue(2);
                if($this->cab->getMaxQueue(2)>1)
                	$this->cab->rotate(array('name'=>$first_inroad['name']),3); // send first queue to hill
                break;
            default: break;
        }
        return true;
    }
    
    function update_number(){
        $post = $this->input->post();
        $this->load->model('cab');
        $cab = $this->cab->select(array('name'=>$post['name']),false);
        $cab['phone'] = $post['phone_number'];
        $this->cab->update($cab);
        redirect(base_url());
    }

    function getPhoneNumber($cname){
        $this->load->model('cab');
        $cab = $this->cab->select(array('name'=>$cname),false);
        $number = !empty($cab)?$cab['phone']:'';
        echo $number;
    }
}

/* End of file twilio_demo.php */