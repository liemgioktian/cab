<form class="form-inline" method="POST" action="<?= site_url('main/setvar') ?>">
    <a href="javascript:send_sms();" class="btn btn-default">Send SMS</a>
    <input type="hidden" name="variable" value="sending_sms" />
    <input type="hidden" name="value" value="<?= $sending_sms=='on'?'off':'on' ?>" />
    <input type="submit" value="Switch <?= $sending_sms=='on'?'Off':'On' ?> SMS" class="btn btn-default">
</form>
<div id="send_sms" style="display: none">
    <form action="<?= site_url('phone/send_sms') ?>" method="POST">
        <span>Recipient : </span>
        <textarea name="body" cols="24" row="2"></textarea>
        <input type="submit" value="send" />
    </form>
</div>