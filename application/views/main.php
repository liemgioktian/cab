<!DOCTYPE html>
<html>
    <head>
        <title>Cabs</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-width=1.0">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <link rel="stylesheet" href="assets/css/jquery.countdown.css">
        <link rel="stylesheet" href="assets/css/henrisusanto.css">
        <?php $is_login = $this->session->userdata('uid')?true:false; ?>
        <script type="text/javascript" >var base_url = '<?= base_url() ?>';</script>
        <script type="text/javascript" src="assets/js/jquery.min.js" ></script>
        <script type="text/javascript" src="assets/js/jquery-ui.js" ></script>
        <script type="text/javascript" src="assets/js/jquery.plugin.js" ></script>
        <script type="text/javascript" src="assets/js/jquery.countdown.js" ></script>
        <script type="text/javascript" src="assets/js/jquery.countdownTimer.js" ></script>
        <script type="text/javascript" src="assets/js/henrisusanto.js" ></script>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 25px">
                <div class="col-xs-12 col-md-4">
                    <a href="<?= base_url() ?>">
                        <img src="<?= base_url('assets/images/logo.png') ?>" alt=""/>
                    </a>
                </div>
                <div class="col-xs-12 col-md-8" id="menu" style="">
                    

                    <?php include $is_login?APPPATH.'/views/logout.php':APPPATH.'/views/login.php'; ?>
                </div>
				

            </div>
			<div class="row" style="margin-top: 25px">
				<?php include APPPATH.'/views/notice-show-edit.php'; ?>
			</div>
            <div class="row" style="margin-top:10px; margin-bottom: 25px;">
                <div class="col-xs-12 col-md-12">
                    <b style="text-transform: uppercase; clear: both"><?= isset($notif)&&$notif!=''?$notif:'' ?></b>
                    <br/>
                    <span id="sms_popup"></span>
                </div>
			</div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <fieldset>
                        <legend><input type="checkbox"/> Unofficial Stand</legend>
                        <ul id="unoff-box">
                            
                        </ul>
                        <div id="unofficial" style="display: none">
                            <input type="radio" name="position" value="1" /> Line <br/>
                            <input type="radio" name="position" value="2" /> Road <br/>
                            <input type="radio" name="position" value="3" /> Hill <br/>
                            <input type="radio" name="position" value="6" /> Transition <br/>
                            <input type="radio" name="position" value="4" /> Airport
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <fieldset>
                        <legend><input type="checkbox"/> J Zone</legend>
                        <ul id="line-box">
                            
                        </ul>
                    </fieldset>
                </div>
                <div class="col-xs-6 col-md-2">
                    <fieldset>
                        <legend><input type="checkbox"/> Road</legend>
                        <ul id="road-box">
                            
                        </ul>
                    </fieldset>
                </div>
                <div class="col-xs-6 col-md-2">
                    <fieldset>
                        <legend><input type="checkbox"/> Stand </legend>
                        <ul id="hill-box">
                            
                        </ul>
                    </fieldset>
                </div>
                <div class="col-xs-6 col-md-2">
                    <fieldset>
                        <legend><input type="checkbox"/> Transition</legend>
                        <ul id="trans-box">
                            
                        </ul>
                    </fieldset>
                </div>
                <div class="col-xs-6 col-md-2">
                    <fieldset>
                        <legend><input type="checkbox"/> Coral</legend>
                        <ul id="air-box">
                            
                        </ul>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if($is_login) include APPPATH.'/views/form-cab.php'; ?>
                </div>
            </div>
        </div>
    </body>
</html>
