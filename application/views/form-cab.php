<form class="form-inline" id="form_cab" method="POST" action="<?= site_url('main/cabs') ?>">
    <input type="text" class="form-control" name="name" id="insert_cab_name" />
    <?php foreach (array('add', 'rotate', 'check status', 'unofficial Stand', 'out of service') as $button): ?>
        <input type="submit" name="<?= $button ?>" id="<?= "button_$button" ?>" value="<?= ucwords($button) ?>" class="btn btn-default" />
    <?php endforeach; ?>
    <button id="btn_phone_number" class="btn btn-default">Phone Number</button>
    <button class="btn btn-warning" onclick="jQuery.get('<?= site_url('main/reform') ?>');return false;">Reform Cab Queue</button>
    <div id="choose_position" style="display: none">
        <input type="radio" name="position" value="1" />J Zone <br/>
        <input type="radio" name="position" value="3" /> Stand <br/>
        <input type="radio" name="position" value="4" /> Coral
    </div>
</form>
<div style="display: none" id="phone_number">
    <form id="form_phone_number" method="POST" action="<?= site_url('phone/update_number') ?>" >
        <input type="hidden" name="name" />
        <input type="text" name="phone_number" style="border: 1px solid black" size="10" />
        <input type="submit" value="Save" />
    </form>
</div>